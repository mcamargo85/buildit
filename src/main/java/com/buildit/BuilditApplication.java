package com.buildit;

import com.buildit.integration.IntegrationService;
import com.buildit.integration.Plant;
import com.buildit.security.CredentialsService;
import com.buildit.security.RequestGateway;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.hateoas.config.EnableHypermediaSupport;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@SpringBootApplication
@EnableHypermediaSupport(type = EnableHypermediaSupport.HypermediaType.HAL)
public class BuilditApplication {

    @Configuration
    static class ObjectMapperCustomizer {
        @Autowired
        @Qualifier("_halObjectMapper")
        private ObjectMapper springHateoasObjectMapper;

        @Bean(name = "objectMapper")
        ObjectMapper objectMapper() {
            return springHateoasObjectMapper
                    .disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES)
                    .configure(DeserializationFeature.READ_DATE_TIMESTAMPS_AS_NANOSECONDS, false)
                    .configure(SerializationFeature.WRITE_DATE_TIMESTAMPS_AS_NANOSECONDS, false)
                    .configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false)
                    .configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false)
                    .registerModules(new JavaTimeModule());
        }
        @Bean
        public RestTemplate restTemplate() {
            RestTemplate _restTemplate = new RestTemplate();
            List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
            messageConverters.add(new MappingJackson2HttpMessageConverter(springHateoasObjectMapper));
            _restTemplate.setMessageConverters(messageConverters);
            return _restTemplate;
        }
    }

    public static void main(String[] args) {
        ConfigurableApplicationContext ctx = SpringApplication.run(BuilditApplication.class, args);
        /* IntegrationService service = ctx.getBean(IntegrationService.class);

        System.out.println(
                service.findPlants("exc", LocalDate.now(), LocalDate.now().plusDays(2))
        ); */

        CredentialsService credentialsService = ctx.getBean(CredentialsService.class);

        System.out.println(credentialsService.getAuthority());
        System.out.println(credentialsService.getAuthorization());

        ClientHttpRequestFactory requestFactory = ctx.getBean(ClientHttpRequestFactory.class);
        RestTemplate restTemplate = new RestTemplate(requestFactory);

        System.out.println("Rest Template Request: " + restTemplate.getForObject( "http://localhost:8090/api/services/resource1", String.class));

        RequestGateway requestGateway = ctx.getBean(RequestGateway.class);
        System.out.println("Integration Flows Request: " + requestGateway.makeRequest("NO Name"));

    }

}
