package com.buildit.security;

import lombok.Getter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import java.util.HashMap;
import java.util.Map;

@Configuration
@EnableConfigurationProperties
@PropertySource("classpath:credentials.properties")
@ConfigurationProperties
@Getter
public class CredentialsService {
    Map<String, String> authority = new HashMap<>();
    Map<String, String> authorization = new HashMap<>();
}