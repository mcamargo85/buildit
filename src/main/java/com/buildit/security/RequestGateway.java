package com.buildit.security;

import org.springframework.integration.annotation.Gateway;
import org.springframework.integration.annotation.MessagingGateway;
import org.springframework.messaging.handler.annotation.Payload;

@MessagingGateway
public interface RequestGateway {
    @Gateway(requestChannel = "reqChannel")
    String makeRequest(@Payload String name);
}
