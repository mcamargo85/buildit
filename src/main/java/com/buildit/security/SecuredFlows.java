package com.buildit.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.integration.http.dsl.Http;

import static org.springframework.http.HttpMethod.GET;

@Configuration
public class SecuredFlows {
    @Bean
    IntegrationFlow flow(ClientHttpRequestFactory factory) {
        return IntegrationFlows.from("reqChannel")
                .handle(Http.outboundGateway("http://localhost:8090/api/services/resource1")
                        .requestFactory(factory)
                        .expectedResponseType(String.class)
                        .httpMethod(GET))
                .get();
    }
}
