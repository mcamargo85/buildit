package com.buildit.rental.application.dto;

import com.buildit.common.domain.model.BusinessPeriod;
import com.buildit.rental.domain.model.POStatus;
import lombok.Data;
import org.springframework.hateoas.Link;

import java.math.BigDecimal;
import java.util.List;

@Data
public class PurchaseOrderDTO {
    private Long id;
    private POStatus status;
    private PlantInventoryEntryDTO plant;
    private BusinessPeriod rentalPeriod;
    private List<Link> links;
    private BigDecimal total;
}
